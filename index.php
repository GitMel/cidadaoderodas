<?php

#mysqli

$servidor = "localhost";
$usuario = "root";
$senha = "";
$database = "cidadaoderodas";

$conexao = mysqli_connect ($servidor,$usuario, $senha ,$database);
#tabela local (idlocal, nomelocal, endereco)
$query = "CREATE TABLE locais (
    idlocal int not null auto_increment,
    nomelocal varchar (100) not null,
    endereco varchar (100) not null,
    ouvidoria varchar (100),
    primary key (idlocal)
)";

$executar = mysqli_query ($conexao, $query);

#tabela ambiente (idambiente, nomeambiente)
$query = "CREATE TABLE ambientes (
    idambiente int not null auto_increment,
    nomeambiente varchar (100) not null,
    primary key (idambiente)
)";
$executar = mysqli_query ($conexao, $query);

#tabela adaptacao(idadaptacao, nomeadaptacao)
$query = "CREATE TABLE adaptacoes (
    idadaptacao int not null auto_increment,
    nomeadaptacao varchar (100) not null,
    primary key (idadaptacao)
)";
$executar = mysqli_query ($conexao, $query);

#tabela local_comentario(idlocalambiente, id_local, id_ambiente)
$query = "CREATE TABLE local_ambiente (
    idlocalambiente int not null auto_increment,
    id_local int not null,
    id_ambiente int not null,

    primary key (idlocalambiente)
)";
$executar = mysqli_query ($conexao, $query);
#tabela local_comentario(idcomentario, id_local, comentario)
$query = "CREATE TABLE local_comentario (
    idcomentario int not null auto_increment,
    id_local int not null,
    comentario varchar (100),

    primary key (idcomentario)
)";
$executar = mysqli_query ($conexao, $query);

#tabela avaliacao_ambiente_local(idavaliacaoambiente, id_ambientelocal, avaliacaoambiente_pos, avaliacaoambiente_neg)
$query = "CREATE TABLE avaliacao_ambiente_local (
    idavaliacaoambiente int not null auto_increment,
    id_ambientelocal int not null,
    avaliacaoambiente_pos int,
    avaliacaoambiente_neg int,
    nao_sei int,
    nao_possui int,
    primary key (idavaliacaoambiente)
)

";
$executar = mysqli_query ($conexao, $query);

#tabela avaliacao_localambiente_adaptacao(idavaliacaoadaptacao, id_localambienteadaptacao, avaliacaoadaptacao_pos, avaliacaoadaptacao_neg)
$query = "CREATE TABLE avaliacao_localambiente_adaptacao(
    idavaliacaoadaptacao int not null auto_increment,
    id_localambienteadaptacao int not null,
    avaliacaoadaptacao_pos int,
    avaliacaoadaptacao_neg int,
    nao_sei int,
    nao_possui int,
    primary key (idavaliacaoadaptacao),
)";
$executar = mysqli_query ($conexao, $query);

#tabela localambiente_adaptacao(idlocalambiente_adaptacao, id_localambiente, id_adaptacao)

$query = "CREATE TABLE localambiente_adaptacao (
    idlocalambienteadaptacao int not null auto_increment,
    id_localambiente int not null,
    id_adaptacao  int not null, 
    primary key (idlocalambienteadaptacao)
)";
$executar = mysqli_query ($conexao, $query);

$possuiambientes=0;
$verificarambiente =  mysqli_query ($conexao,"SELECT ambientes.nomeambiente FROM ambientes");
while($verificando = mysqli_fetch_array($verificarambiente)){
    if($verificando["nomeambiente"] != null){
        $possuiambientes = 1;
    }
}
if($possuiambientes==0){
    #Adiicionando tipos de ambiente
    $query = "INSERT INTO ambientes (nomeambiente) VALUES ('Estacionamento');";
    $executar = mysqli_query ($conexao, $query);
    $query = "INSERT INTO ambientes (nomeambiente) VALUES ('Corredor');";
    $executar = mysqli_query ($conexao, $query);
    $query = "INSERT INTO ambientes (nomeambiente) VALUES ('Prevenção de incêndio');";
    $executar = mysqli_query ($conexao, $query);
    $query = "INSERT INTO ambientes (nomeambiente) VALUES ('Calçada');";
    $executar = mysqli_query ($conexao, $query);
    $query = "INSERT INTO ambientes (nomeambiente) VALUES ('Banheiro');";
    $executar = mysqli_query ($conexao, $query);
    $query = "INSERT INTO ambientes (nomeambiente) VALUES ('Recepção');";
    $executar = mysqli_query ($conexao, $query);
    
}
$possuiadaptacao=0;
$verificaradaptacao =  mysqli_query ($conexao,"SELECT adaptacoes.nomeadaptacao FROM adaptacoes");
while($verificando = mysqli_fetch_array($verificaradaptacao)){
    if($verificando["nomeadaptacao"] != null){
        $possuiadaptacao = 1;
    }
}
if($possuiadaptacao==0){
#Adicionando tipos de adaptacao
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Espaço adicional pintado ao lado da vaga');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Vaga próxima ao acesso do prédio');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Símbolo de Acesso no piso');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Vagas reservadas');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Símbolo de Acesso em placa vertical');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Rampa ligando a vaga à calçada');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Caminho sem interrupções');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Permite volta de 180º');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Placas informativas pelo prédio');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Sem inclinação');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Piso com superfície regular');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Largura mínima (120m)');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Trâsito de pedestre livre');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Porta abre para fora');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Maçaneta ou trinco do tipo alavanca');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Área livre ao redor do vaso sanitário');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Espaço para espera fora da área de circulação');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Balcões abaixo de 0,91 m');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Balcões que permitem aproximação frontal');";
$executar = mysqli_query ($conexao, $query);
$query = "INSERT INTO adaptacoes (nomeadaptacao) VALUES ('Bebedouro abaixo de 0,91 m');";
$executar = mysqli_query ($conexao, $query);

}


include 'db.php';
include 'header.php';

if (isset($_GET['pagina'])){
    $pagina = $_GET['pagina'];
}else{
    $pagina = 'home';
}
if($pagina =='avaliacao'){
    include 'view/avaliacao.php';
}elseif($pagina =='processa_avaliacao'){
    include 'processa_avaliacao.php';
}elseif( $pagina == 'inserir_local'){
    include 'view/inserir_local.php';
}elseif( $pagina == 'processa_local'){
    include 'processa_local.php';
}elseif( $pagina == 'processa_img'){
    include 'processa_img.php';
}elseif( $pagina == 'procurarLocal'){
    include 'procurarLocal.php';
}elseif( $pagina == 'processa_avaliacao_adaptacao'){
    include 'processa_avaliacao_adaptacao.php';
}elseif( $pagina == 'inserir_img'){
    include 'view/inserir_img.php';
}
else {
    include 'view/home.php';
};