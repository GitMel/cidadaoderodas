<div class="container-forms">
    <div class="container d-flex flex-column align-items-center justify-content-center">
        <div class="d-flex flex-column">
            <div class="card card-grande">
                <div class="card-body d-flex flex-column justify-content-between">
                    <h5 class="card-title">Cadastro de local</h5>
                    <div class="card-text">
                        <form class="d-flex flex-column justify-content-between" enctype="multipart/form-data" method="post" action="processa_local.php">
                            <div class="inputs">
                                <label for="nomelocal">Nome do local</label>
                                <input class="form-control" type="text" name="nomelocal">
                                <label for="endereco">Endereço</label>
                                <input class="form-control" type="text" name="endereco">
                                <label for="ouvidoria">Link ouvidoria</label>
                                <input class="form-control" type="url" name="ouvidoria">
                                <label for="conteudo">Enviar imagem:</label>
                                <input type="file" name="pic" accept="image/*"> 
                            </div>
                            <input class="btn btn-verde mt-3" type="submit" value="Cadastrar">
                        </form>                                                             
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
