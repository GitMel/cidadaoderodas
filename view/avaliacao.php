<?php
echo '
<div class="container avaliacao" id="container-avaliacao">
   <div class="row">
      ';
      while ($locais = mysqli_fetch_array($consulta2)) {
      $filenamejpg = 'uploads/' . $locais['nomelocal'] . '.jpg';
      $filenamepng = 'uploads/' . $locais['nomelocal'] . '.png';
      if ($locais['idambiente'] == 1) {
      echo '
      <div class =" col-md-6 col-lg-4 mt-4">
         <div class="card">
            <div class="card-header">
            <div class = "d-flex flex-row justify-content-between mb-2">
            <h5 class="card-title">' . $locais['nomelocal'] . '
            </h5>
            <div class="ouvidoria">
            <a href="' . $locais['ouvidoria'] . '" target="_blank" class="d-flex flex-row">
            <i class="las la-bullhorn ">
            </i>  
            <p class="ml-2 text-ouvidoria">Ouvidoria
            </p>
            </a>
            </div>
            </div>
            <div class="thumb">';
            if (file_exists($filenamejpg)) {
                echo '    
                <img src="uploads/' . $locais['nomelocal'] . '.jpg" class="portrait">';
            } elseif (file_exists($filenamepng)) {
                echo '    
                <img src="uploads/' . $locais['nomelocal'] . '.png" class="portrait">';
            }
            echo '
            </div>
            <p>' . $locais['endereco'] . '
            </p>
            <div class="d-flex flex-row justify-content-center">
            <a id="' . $locais['idlocal'] . '" class="card-link">
                <i class="las la-angle-double-down"></i>
            </a>
            </div>
        </div>
            <div id="card-'.$locais['idlocal'].'" class="card-body">
               
               <div class="d-flex flex-row justify-content-end">
                  <span class="badge badge-pill badge-success m-1"> Acessível  </span>                                                    
                  <span class="badge badge-pill badge-danger m-1">Inacessível </span>                          
                  <span class="badge badge-pill badge-dark m-1">Não possui </span>
               </div>
               <table class="table table-borderless">
                  <thead>
                     <tr>
                        <th scope="col">
                        </th>
                        <th class="scope-col" scope="col">
                            <span class="badge badge-redondo badge-success ">  </span>
                        </th>
                        <th class="scope-col" scope="col">
                           <span class="badge badge-redondo badge-danger ">  </span>
                        </th>
                        <th scope="col">
                           <span class="badge badge-redondo badge-dark ">  </span>
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <th scope="row">' . $locais['nomeambiente'] . '
                        </th>
                        <td>' . $locais['avaliacaoambiente_pos'] . ' </td>
                        <td>' . $locais['avaliacaoambiente_neg'] . ' </td>
                        <td>' . $locais['nao_possui'] . ' </td>
                     </tr>
                     </li>';
                     } else if ($locais['idambiente'] < 6) {
                     echo '
                     <tr>
                        <th scope="row">' . $locais['nomeambiente'] . '
                        </th>
                        <td>' . $locais['avaliacaoambiente_pos'] . ' </td>
                        <td>' . $locais['avaliacaoambiente_neg'] . ' </td>
                        <td>' . $locais['nao_possui'] . ' </td>
                     </tr>
                     ';
                     } else {
                     echo '
                     <tr>
                        <th scope="row">' . $locais['nomeambiente'] . '
                        </th>
                        <td>' . $locais['avaliacaoambiente_pos'] . ' </td>
                        <td>' . $locais['avaliacaoambiente_neg'] . ' </td>
                        <td>' . $locais['nao_possui'] . ' </td>
                     </tr>
                  </tbody>
               </table>
               <div class=" d-flex flex-row align-items-center">
                  <form class="col-6 p-0" method="post" action="procurarLocal.php">
                     <input  type="hidden"  name="nomelocal" value="'.$locais['nomelocal'].'">
                     <input  type="hidden"  name="idlocal" value="'.$locais['idlocal'].'">
                  <input class="btn btn-vermelho mr-2" type="submit"  value="Mais info.">
                  </form>
                  <button type="button" class="btn btn-verde col-6" data-toggle="modal" data-target="#modal' . $locais['idlocal'] . '">Avaliar </button>
               </div>
            </div>
         </div>
      </div>
      ';
      }
      }
      echo ' 
   </div>
</div>
';


//==========================INICIO MODAL avaliar AMBIENTES========================================
while ($modal = mysqli_fetch_array($consulta_modal)) {
if ($modal['idambiente'] == 1) {
echo '
<div class="modal fade" id="modal' . $modal['idlocal'] . '" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">' . $modal['nomelocal'] . '
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times; </span>
            </button>
         </div>
         <form method="post" action="processa_avaliacao.php">
            <div class="modal-body">
               <h4>' . $modal['nomeambiente'] . ' 
                  <br>
               </h4>
               <div class="form-check d-flex flex-column flex-md-row" >
                  <div>
                     <input class="form-check-input" type="radio" name="' . $modal['idlocalambiente'] . '"  value="avaliacaoambiente_pos" >
                     <label name="avaliacaoambiente_po" value="1" class="form-check-label mr-4" for="">
                     Acessível </label>
                  </div>
                  <div>
                     <input class="form-check-input" type="radio" name="' . $modal['idlocalambiente'] . '"  value="avaliacaoambiente_neg" >
                     <label name="avaliacaoambiente_ne" value="1" class="form-check-label mr-4" for="">Não acessível </label>
                  </div>
                  <div >
                     <input class="form-check-input" type="radio" name="' . $modal['idlocalambiente'] . '"  value="nao_possui" >
                     <label name="avaliacaoambiente_ne" value="1" class="form-check-label mr-4" for="">
                     Não possui </label>
                  </div>
               </div>
               ';
               } else if ($modal['idambiente'] 
               < 6) {
               echo '
               <h4>' . $modal['nomeambiente'] . ' 
                  <br>
               </h4>
               <div class="form-check d-flex flex-column flex-md-row" >
                  <div>
                     <input class="form-check-input" type="radio" name="' . $modal['idlocalambiente'] . '"  value="avaliacaoambiente_pos" >
                     <label name="avaliacaoambiente_po" value="1" class="form-check-label mr-4" for="">
                     Acessível </label>
                  </div>
                  <div>
                     <input class="form-check-input" type="radio" name="' . $modal['idlocalambiente'] . '"  value="avaliacaoambiente_neg" >
                     <label name="avaliacaoambiente_ne" value="1" class="form-check-label mr-4" for="">Não acessível </label>
                  </div>
                  <div>
                     <input class="form-check-input" type="radio" name="' . $modal['idlocalambiente'] . '"  value="nao_possui" >
                     <label name="avaliacaoambiente_ne" value="1" class="form-check-label mr-4" for="">
                     Não possui </label>
                  </div>
               </div>
               ';
               } else {
               echo '
               <h4>' . $modal['nomeambiente'] . ' 
                  <br>
               </h4>
               <div class="form-check d-flex flex-column flex-md-row" >
                  <div>
                     <input class="form-check-input" type="radio" name="' . $modal['idlocalambiente'] . '"  value="avaliacaoambiente_pos" >
                     <label name="avaliacaoambiente_po" value="1" class="form-check-label mr-4" for="">
                     Acessível </label>
                  </div>
                  <div>
                     <input class="form-check-input" type="radio" name="' . $modal['idlocalambiente'] . '"  value="avaliacaoambiente_neg" >
                     <label name="avaliacaoambiente_ne" value="1" class="form-check-label mr-4" for="">
                     Não acessível </label>
                  </div>
                  <div>
                     <input class="form-check-input" type="radio" name="' . $modal['idlocalambiente'] . '"  value="nao_possui" >
                     <label name="avaliacaoambiente_ne" value="1" class="form-check-label mr-4" for="">
                     Não possui </label>
                  </div>
               </div>
               <div class="form-group mt-5">
               <label for="comentario">Deixe seu comentário</label>
               <input type="text" class="form-control" name="comentario" placeholder="Comentário não obrigatório">
               <input type="hidden"   name="idlocal" value="' . $modal['idlocal'] . '">   
            </div>
            </div>
            
            <div class=" d-flex flex-row">
               <button type="button" class="btn btn-cinza m-1" data-dismiss="modal">Fechar </button>
               <input class="btn btn-verde m-1" type="submit" value="Avaliar">
            </div>
      </div>
      
   </div>
   </form>
</div>
';
};
}


  ?>
  <script>
        function fechar() {
        $(".card-body").each(function () {
            $(this).css({ "opacity": "0",  "height": "0" })
        })
        $(".open").each(function(){
            $(this).removeAttr('onclick');
            $(this).removeClass('open');
        })
    }
  </script>                      