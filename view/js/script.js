$(function () {
    $('.ouvidoria').hover(function () {
        $(this).css({ "width": " 45%" });
        $(this).addClass('atual');
        $('.atual .text-ouvidoria').css({ "color": " white", "display": "block" });

    }, function () {
        $(this).css({ "width": " 15%" });
        $(".atual .text-ouvidoria").css({ "color": " transparent", "display": "none" });
        $(this).removeClass('atual');

    })

    $(".card-link").on('click', function () {
        let id = $(this).attr('id');
      
        $(this).attr('onClick', 'fechar()');
        $(this).addClass('open');
        $(".card-body").each(function () {
            $(this).css({ "opacity": "0",  "height": "0" })
        })
        $(`#card-${id}`).css({ "opacity": "1",  "height": "inherit" })

    })

})

